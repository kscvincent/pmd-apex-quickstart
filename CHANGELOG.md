# pmd-apex analyzer changelog

## v3.1.2
- Update command version (!90)

## v3.1.1
- Updated PMD to [v6.47.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.47.0) (!87)
  - [core] address common-io path traversal vulnerability (CVE-2021-29425)

## v3.1.0
- Upgrade core analyzer dependencies (!88)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.1
- Updated PMD to [v6.45.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.45.0) (!86)

## v3.0.0
- Bump to next major version `v3.0.0` (!85)

## v2.12.15
- Update `command` to v1.7.0 (!81)

## v2.12.14
- Updated PMD to [v6.44.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.44.0) (!79)

## v2.12.13
- Updated PMD to [v6.43.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.43.0) (!78)
- Update go dependencies (!78)

## v2.12.12
- Update go dependencies (!78)
  - Support ruleset overrides

## v2.12.11
- Updated PMD to [v6.42.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.42.0) (!77)

## v2.12.10
- Remove `pmd-java` submodule entirely (!75)

## v2.12.9
- Enable `log4j2.formatMsgNoLookups` by default (!74)

## v2.12.8
- Updated PMD to [v6.40.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.40.0) (!72)
  - The Apex language support has been bumped to version 54.0
  - Various improvements and bugfixes for Apex ruleset

## v2.12.7
- Upgrade go to v1.17 (!71)

## v2.12.6
- Update PMD to [v6.39.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.39.0) (!69)

## v2.12.5
- Update PMD to [v6.38.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.38.0) (!68)
  - fix: SOQL performed in a for-each loop doesn't trigger ApexCRUDViolationRule
  - fix: ApexCRUDViolationRule maintains state across files
  - fix: ApexCRUDViolationRule - add super call

## v2.12.4
- Update PMD to [v6.36.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.36.0) (!66)
  - Improved Incremental Analysis performance

## v2.12.3
- Update PMD to [v6.35.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/6.35.0) (!61)
  - fix: [[apex] Correct findBoundary when traversing AST](https://github.com/pmd/pmd/pull/3243)

## v2.12.2
- Update PMD to [v6.34.0](https://github.com/pmd/pmd/releases/tag/pmd_releases%2F6.34.0) (!58)
  - The Apex rule `ApexCRUDViolation` does not ignore getters anymore and also flags
  - fix: `ApexCRUDViolationRule` fails to report CRUD violation on COUNT() queries
  - fix: `ApexCRUDViolationRule` false-negative on non-VF getter
  - `ApexCRUDViolationRule`: Do not assume method is VF getter to avoid CRUD checks
  - `ApexCRUDViolation`: COUNT is indeed CRUD checkable since it exposes data (false-negative)

## v2.12.1
- Update PMD to v6.33.0 (!57)

## v2.12.0
- Update report dependency in order to use the report schema version 14.0.0 (!54)

## v2.11.2
- Update PMD to v6.32.0 (!53)

## v2.11.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!51)

## v2.11.0
- Update base image to adoptopenjdk/openjdk15:alpine (!50)
- Update PMD to v6.30.0 (!50)

## v2.10.0
- Update common to v2.22.0 (!49)
- Update urfave/cli to v2.3.0 (!49)

## v2.9.0
- Update PMD to v6.29.0 (!48)
- Update golang dependencies logrus to v1.6.0, cli to v1.22.5 (!48)

## v2.8.0
- Update common and enabled disablement of rulesets (!47)

## v2.7.1
- Update golang dependencies (!41)

## v2.7.0
- Upgrade PMD to 6.27.0 (!39)
- Update golang dependencies (!39)

## v2.6.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!37)

## v2.5.1
- Update golang to v1.15 (!34)
- Bump PMD to 6.26.0

## v2.5.0
- Add scan object to report (!30)

## v2.4.3
- Dedupe report findings (!28)

## v2.4.2
- Safe handle plugin.Match file closer (!29)

## v2.4.1
- Bump PMD to 6.25.0 (!24)

## v2.4.0
- Switch to the MIT Expat license (!23)

## v2.3.1
- Update Debug output to give a better description of command that was ran (!21)

## v2.3.0
- Update logging to be standardized across analyzers (!20)

## v2.2.4
- Bump PMD to 6.24.0 (!18)

## v2.2.3
- Remove `location.dependency` from the generated SAST report (!17)

## v2.2.2
- Bump PMD to 6.23.0, Docker image to Java 13 (!16)

## v2.2.1
- Bump PMD to 6.22.0 (!14)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!11)

## v2.1.0
- Add support for custom CA certs (!9)

## v2.0.1
- Bump PMD to 6.17.0

## v2.0.0
- Initial release
