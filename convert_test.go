package main

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func TestConvert(t *testing.T) {
	in := `<?xml version="1.0" encoding="UTF-8"?>
	<pmd xmlns="http://pmd.sourceforge.net/report/2.0.0"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://pmd.sourceforge.net/report/2.0.0 http://pmd.sourceforge.net/report_2_0_0.xsd"
     version="6.15.0" timestamp="2019-06-22T05:14:58.320">
	    <file name="/tmp/app/src/classes/MetadataDataController.cls">
    	    <violation beginline="60" endline="60" begincolumn="20" endcolumn="23" rule="ApexXSSFromURLParam" ruleset="Security" externalInfoUrl="https://pmd.github.io/pmd/pmd_rules_apex_security.html#apexxssfromurlparam" priority="3">
        	    Apex classes should escape/sanitize Strings obtained from URL parameters
	        </violation>
    	    <violation beginline="123" endline="123" begincolumn="27" endcolumn="41" rule="ApexSuggestUsingNamedCred" ruleset="Security" externalInfoUrl="https://pmd.github.io/pmd/pmd_rules_apex_security.html#apexsuggestusingnamedcred" priority="3">
        	    Suggest named credentials for authentication
	        </violation>
    	</file>
	    <file name="/tmp/app/src/classes/MetadataServicePatcher.cls">
	   	    <violation beginline="299" endline="301" begincolumn="4" endcolumn="44" rule="ApexCRUDViolation" ruleset="Security" externalInfoUrl="https://pmd.github.io/pmd/pmd_rules_apex_security.html#apexcrudviolation" priority="3">
    	   	    Validate CRUD permission before SOQL/DML operation
			</violation>
   	    	<violation beginline="562" endline="562" begincolumn="25" endcolumn="100" rule="ApexCRUDViolation" ruleset="Security" externalInfoUrl="https://pmd.github.io/pmd/pmd_rules_apex_security.html#apexcrudviolation" priority="3">
       	    	Validate CRUD permission before SOQL/DML operation
	        </violation>
   		</file>
	</pmd>`

	var scanner = metadata.IssueScanner

	r := strings.NewReader(in)
	want := &report.Report{
		Version: report.CurrentVersion(),
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "ApexXSSFromURLParam",
				Message:     "ApexXSSFromURLParam",
				Description: "Makes sure that all values obtained from URL parameters are properly escaped / sanitized to avoid XSS attacks.",
				Solution:    "Apex classes should escape/sanitize Strings obtained from URL parameters",
				CompareKey:  "PMD:Security-ApexXSSFromURLParam:/tmp/app/src/classes/MetadataDataController.cls:60:20",
				Severity:    report.SeverityLevelMedium,
				Location: report.Location{
					File:      "/tmp/app/src/classes/MetadataDataController.cls",
					LineStart: 60,
					LineEnd:   60,
				},
				Identifiers: []report.Identifier{
					report.Identifier{
						Type:  "pmd_apex_rule_id",
						Name:  "PMD Apex Rule ID Security-ApexXSSFromURLParam",
						Value: "Security-ApexXSSFromURLParam",
						URL:   "https://pmd.github.io/pmd/pmd_rules_apex_security.html#apexxssfromurlparam",
					},
				},
				Links: []report.Link{},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "ApexSuggestUsingNamedCred",
				Message:     "ApexSuggestUsingNamedCred",
				Description: "Detects hardcoded credentials used in requests to an endpoint. You should refrain from hardcoding credentials: They are hard to mantain by being mixed in application code, Particularly hard to update them when used from different classes, Granting a developer access to the codebase means granting knowledge of credentials, keeping a two-level access is not possible, Using different credentials for different environments is troublesome and error-prone. Instead, you should use Named Credentials and a callout endpoint.",
				Solution:    "Suggest named credentials for authentication",
				CompareKey:  "PMD:Security-ApexSuggestUsingNamedCred:/tmp/app/src/classes/MetadataDataController.cls:123:27",
				Severity:    report.SeverityLevelMedium,
				Location: report.Location{
					File:      "/tmp/app/src/classes/MetadataDataController.cls",
					LineStart: 123,
					LineEnd:   123,
				},
				Identifiers: []report.Identifier{
					report.Identifier{
						Type:  "pmd_apex_rule_id",
						Name:  "PMD Apex Rule ID Security-ApexSuggestUsingNamedCred",
						Value: "Security-ApexSuggestUsingNamedCred",
						URL:   "https://pmd.github.io/pmd/pmd_rules_apex_security.html#apexsuggestusingnamedcred",
					},
				},
				Links: []report.Link{
					report.Link{
						Name: "",
						URL:  "https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_callouts_named_credentials.htm",
					},
				},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "ApexCRUDViolation",
				Message:     "ApexCRUDViolation",
				Description: "The rule validates you are checking for access permissions before a SOQL/SOSL/DML operation. Since Apex runs in system mode not having proper permissions checks results in escalation of privilege and may produce runtime errors. This check forces you to handle such scenarios.",
				Solution:    "Validate CRUD permission before SOQL/DML operation",
				CompareKey:  "PMD:Security-ApexCRUDViolation:/tmp/app/src/classes/MetadataServicePatcher.cls:299:4",
				Severity:    report.SeverityLevelMedium,
				Location: report.Location{
					File:      "/tmp/app/src/classes/MetadataServicePatcher.cls",
					LineStart: 299,
					LineEnd:   301,
				},
				Identifiers: []report.Identifier{
					report.Identifier{
						Type:  "pmd_apex_rule_id",
						Name:  "PMD Apex Rule ID Security-ApexCRUDViolation",
						Value: "Security-ApexCRUDViolation",
						URL:   "https://pmd.github.io/pmd/pmd_rules_apex_security.html#apexcrudviolation",
					},
				},
				Links: []report.Link{},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "ApexCRUDViolation",
				Message:     "ApexCRUDViolation",
				Description: "The rule validates you are checking for access permissions before a SOQL/SOSL/DML operation. Since Apex runs in system mode not having proper permissions checks results in escalation of privilege and may produce runtime errors. This check forces you to handle such scenarios.",
				Solution:    "Validate CRUD permission before SOQL/DML operation",
				CompareKey:  "PMD:Security-ApexCRUDViolation:/tmp/app/src/classes/MetadataServicePatcher.cls:562:25",
				Severity:    report.SeverityLevelMedium,
				Location: report.Location{
					File:      "/tmp/app/src/classes/MetadataServicePatcher.cls",
					LineStart: 562,
					LineEnd:   562,
				},
				Identifiers: []report.Identifier{
					report.Identifier{
						Type:  "pmd_apex_rule_id",
						Name:  "PMD Apex Rule ID Security-ApexCRUDViolation",
						Value: "Security-ApexCRUDViolation",
						URL:   "https://pmd.github.io/pmd/pmd_rules_apex_security.html#apexcrudviolation",
					},
				},
				Links: []report.Link{},
			},
		},
		Analyzer:        "pmd-apex",
		DependencyFiles: []report.DependencyFile{},
	}
	got, err := convert(r, "/")
	if err != nil {
		t.Fatal(err)
	}
	got.Config = ruleset.Config{}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
