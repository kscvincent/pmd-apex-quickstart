# pmd-apex analyzer

GitLab Analyzer for [Apex](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_intro_what_is_apex.htm) projects.

This analyzer is a wrapper around [pmd](https://pmd.github.io), a static code analyzer, utilizing its [Apex security rules](https://pmd.github.io/pmd-6.22.0/pmd_rules_apex_security.html).

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Using this analyzer

This analyzer looks for two specific files to determine if it should run against a repository.

* `sfdx-project.json`
  * If this file exists, the analyzer will match and scan the project.
* `package.xml`
  * If this file contains any nodes containing `<name>ApexClass</name>`, the analyzer will match and scan the project.

## Versioning and release process

Please check the common [Versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common/blob/update_doc/README.md#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
