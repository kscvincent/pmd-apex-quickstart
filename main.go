package main

import (
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
	"gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v2/plugin"
)

func main() {
	app := command.NewApp(metadata.ReportAnalyzer)
	app.Usage = metadata.AnalyzerUsage

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		Analyzer:     metadata.ReportAnalyzer,
		AnalyzeFlags: analyzeFlags(),
		AnalyzeAll:   true,
		Convert:      convert,
		Scanner:      metadata.ReportScanner,
		ScanType:     metadata.Type,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
