package plugin

import (
	"os"
	"path/filepath"
	"testing"
)

func TestMatch(t *testing.T) {
	absPath, err := filepath.Abs("..")
	if err != nil {
		t.Error(err)
	}

	var tcs = []struct {
		Path string
		Want bool
	}{
		{absPath + "/test/fixtures/app/src/package.xml", true},
		{absPath + "/test/fixtures/sfdx-app/sfdx-project.json", true},
		{absPath + "/test/fixtures/broken/src/package.xml", false},
		{absPath + "/test/fixtures/noclass/src/package.xml", false},
	}

	for _, tc := range tcs {
		fi, err := os.Stat(tc.Path)
		if err != nil {
			t.Error(err)
			continue
		}
		got, err := Match(tc.Path, fi)
		if err != nil {
			t.Error(err)
			continue
		}

		if got != tc.Want {
			t.Errorf("Wrong result for %s: expecting %v but got %v", tc.Path, tc.Want, got)
		}
	}
}
